using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Vacuum : MonoBehaviour
    {
        [SerializeField] private float m_VacuumForce = 300f;
        [SerializeField] private float m_Drag = 20;

        private AudioSource m_Audio;
        [SerializeField] private AudioClip m_whooshSound;

        public float DragCoefficient { get {return m_Drag;} }

        public bool IsActive = true;

        private void Start()
        {
            m_Audio = GetComponent<AudioSource>();
        }

        // Scale force with distance, like gravity
        public Vector2 GetForce(Projectile p)
        {
            Vector2 separation = transform.position - p.transform.position;
            Vector2 pull = (m_VacuumForce * transform.parent.localScale.x * separation.normalized) / Mathf.Pow(separation.magnitude, 2);

            // If vacuum is inactive, return no force
            pull *= IsActive ? 1 : 0;

            return pull;
        }

        public void StopSound()
        {
            m_Audio.Stop();
        }

        // Tag any projectile that enters the area of attraction
        void OnTriggerEnter2D(Collider2D c)
        {
            Projectile p = c.gameObject.GetComponent<Projectile>();

            p?.Attract(this);

            if (m_whooshSound)
            {
                m_Audio?.PlayOneShot(m_whooshSound, Utility.Audio.AudioManager.VOLUME_SFX);
            }
        }
    }
}