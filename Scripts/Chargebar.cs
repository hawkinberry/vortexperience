using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Chargebar : MonoBehaviour
{
    public List<Sprite> m_stages;

    private SpriteRenderer m_output;

    private void Start()
    {
        m_output = GetComponent<SpriteRenderer>();
        Hide();
    }

    public void SetFill(float percent)
    {
        int idx = (int)((m_stages.Count - 1) * percent);
        m_output.sprite = m_stages[idx];
    }

    public void Hide()
    {
        m_output.sprite = null;
    }
}
