using UnityEngine;
//using TMPro;
using System.Collections.Generic;

namespace Vortex
{
    using System;
    using Utility.Audio;

    public class Player : Character
    {
        public static int layerMask;

        private bool m_isEnabled;

        [SerializeField] private Projectile projectile;

        [Header("Throwing")]
        public Chargebar m_chargeBar;
        public AnimatorOverrideController m_ControllerDefault;
        public AnimatorOverrideController m_ControllerWithBall;
        [SerializeField] private List<AudioClip> throwSounds;
        [SerializeField] private bool hasObject;
        public bool IsHoldingObject
        {
            get { return hasObject; }
        }

        private float pullback = 0.0f;
        public float Pullback
        {
            get { return pullback; }
            set
            {
                if (hasObject || value == 0.0f)
                {
                    pullback = value;
                }

                if (m_isEnabled)
                {
                    // Set the animator value so we do the animation anyway
                    m_Animator.SetFloat(PLAYER_ANIMATION_PULL, value);
                }
            }
        }

        public void SetCharge(float percent)
        {
            if (hasObject)
            {
                m_chargeBar.SetFill(percent);
            }
            else
            {
                m_chargeBar.Hide();
            }
        }

        [Header("Audio")]
        private AudioSource m_Audio;
        [SerializeField] private AudioClip footstep;
        private bool m_SoundPlaying = false;
        private float m_StepInterval;

        // Determine whether player should be considered to be moving
        public bool isMoving
        {
            get { return currentVelocity.magnitude > 0.01f; }
        }

        // Determine whether a collider is owned by the player
        public static bool IsPlayer(Collider2D c)
        {
            return c.gameObject.name.Contains("Player");
        }

        private void OnEnable()
        {
            // Events to restrict player movement
            Utility.Executive.OnGameResume += Enable;
            Utility.Executive.OnGamePause += Disable;
            Utility.Executive.OnGameOver += Disable;
        }

        private void OnDisable()
        {
            Utility.Executive.OnGameResume -= Enable;
            Utility.Executive.OnGamePause -= Disable;
            Utility.Executive.OnGameOver -= Disable;
        }

        // Start is called before the first frame update
        protected override void Start()
        {
            Enable();
            layerMask = 1 << LayerMask.NameToLayer("Player");

            base.Start();

            if (m_Animator)
            {
                AnimationClip[] clips = m_Animator.runtimeAnimatorController.animationClips;
                foreach (AnimationClip a in clips)
                {
                    if (a.name.Contains("walk"))
                    {
                        m_StepInterval = a.length / 2f;
                    }
                }
            }

            if (m_Audio = GetComponent<AudioSource>())
            {
                AudioManager.RegisterSFXSource(ref m_Audio);
            }
        }

        private void Enable()
        {
            m_isEnabled = true;
        }

        protected override void Disable()
        {
            m_isEnabled = false;
            CancelInvoke("PlayStep");
            base.Disable();
        }

        private void Walking(bool enable)
        {
            if (enable)
            {
                if (!m_SoundPlaying)
                {
                    m_SoundPlaying = true;
                    InvokeRepeating("PlayStep", 0f, m_StepInterval);
                }
            }
            else
            {
                m_SoundPlaying = false;
                CancelInvoke("PlayStep");
            }
        }

        // Called from InvokeRepeating
        private void PlayStep()
        {
            if (footstep)
            {
                m_Audio.PlayOneShot(footstep, AudioManager.VOLUME_SFX * 0.3f);
            }
        }

        private void PlayThrowSound()
        {
            AudioManager.PlayRandomSound(ref throwSounds, Utility.Audio.AudioManager.VOLUME_SFX);
        }

        #region Character Methods
        public override void Move(Vector2 direction)
        {
            if (m_isEnabled)
            {
                CalculateVelocity(direction);

                Walking(isMoving);

                // filtering allows us to support multiple controllers at once
                if (direction == Vector2.zero)
                {
                    return;
                }

                DoMove(currentVelocity);
            }
        }
        #endregion


        public void Cancel()
        {
            m_Animator.SetTrigger(PLAYER_ANIMATION_CANCEL_TRIGGER);
        }

        public void Throw(Vector3 velocity)
        {
            if (m_isEnabled)
            {
                m_Animator.SetTrigger(PLAYER_ANIMATION_THROW_TRIGGER);
                PlayThrowSound();
            }

            if (hasObject)
            {
                // If throwing left but facing right
                // If throwing right but facing left
                if ((velocity.x < 0) == m_FacingRight)
                {
                    //velocity.x *= -1;
                    Flip(); // face in direction of throw
                }

                // Add to scene - a little in front of player so we don't run collision logic
                float nudge = 1f;
                Vector3 spawnPos = transform.position + (velocity.normalized * nudge);
                GameObject obj = GameObject.Instantiate(projectile.gameObject, spawnPos, Quaternion.identity, transform.parent);

                // Give velocity - point in direction character is facing
                obj.GetComponent<Projectile>().Launch(velocity);

                hasObject = false;
                m_Animator.runtimeAnimatorController = m_ControllerDefault;
                m_chargeBar.Hide();
            }
        }

        public void HoldObject(Projectile obj)
        {
            if (!hasObject)
            {
                hasObject = true;
                Destroy(obj.gameObject);

                m_Animator.runtimeAnimatorController = m_ControllerWithBall;
            }
        }
    }
}
