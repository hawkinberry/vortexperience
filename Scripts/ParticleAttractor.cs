using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{

    public class ParticleAttractor : MonoBehaviour
    {
        public Particle m_ParticlePrefab;
        public float m_particleSpawnRate = 0.2f;
        private float timeSinceSpawn;

        public int MAX_PARTICLES = 100;
        private List<Particle> m_ActiveParticles;
        private List<Particle> m_Pool;
        private int numParticles;

        public float maxSpread = 5f;
        private float m_spread;
        public float Scale;

        public float vacuumForce = 400f;
        private Collider2D m_Horizon;

        public bool isActive = false;

        void Awake()
        {
            m_Horizon = GetComponent<Collider2D>();

            m_Pool = new List<Particle>(MAX_PARTICLES);
            m_ActiveParticles = new List<Particle>(MAX_PARTICLES);

            timeSinceSpawn = 0f;
            numParticles = 0;
            m_spread = maxSpread;
            Scale = 1;
        }

        private void Start()
        {
            for (int i = 0; i < MAX_PARTICLES; i++)
            {
                m_Pool.Add(Instantiate(m_ParticlePrefab, transform));
                m_Pool[i].Hide();
            }

            Enable();
        }

        // Update is called once per frame
        void Update()
        {
            if (isActive)
            {
                timeSinceSpawn += Time.deltaTime;

                // spawn new particle
                if (numParticles < MAX_PARTICLES &&
                    timeSinceSpawn >= m_particleSpawnRate)
                {
                    SpawnParticle();
                }

                numParticles = m_ActiveParticles.Count;
            }
        }

        void FixedUpdate()
        {
            if (isActive)
            {
                UpdateParticles();
            }
        }

        public void Enable()
        {
            isActive = true;
        }

        public void Disable()
        {
            //isActive = false;
            vacuumForce *= -1;
            foreach (Particle p in m_ActiveParticles)
            {
                p.Inert();
            }
        }

        private void SpawnParticle()
        {
            // pick random location along unit circle

            if (numParticles < MAX_PARTICLES)
            {
                // Transfer from pool to active
                Particle newParticle = ActivateParticle();

                // init
                Vector2 spawnPosition = Random.insideUnitCircle.normalized * Random.Range(0.8f, m_spread * Scale);
                newParticle.transform.localPosition = spawnPosition;

                timeSinceSpawn = 0f;
            }
        }

        private void UpdateParticles()
        {
            // move existing particles - go backwards as we might remove them as they hit the center
            for (int i = m_ActiveParticles.Count - 1; i >= 0; i--)
            {
                Particle p = m_ActiveParticles[i];

                p.AddForce(GetForce(p));

                if (p.Collider.IsTouching(m_Horizon))
                {
                    ReleaseParticle(p);
                }
            }
        }

        private Vector2 GetForce(Particle p)
        {
            Vector2 separation = transform.position - p.transform.position;
            //Vector2 pull = (vacuumForce * transform.parent.localScale.x * separation.normalized) / Mathf.Pow(separation.magnitude, 2);
            Vector2 pull = (vacuumForce * separation.normalized) / Mathf.Pow(separation.magnitude, 2);

            return pull;
        }

        private void ReleaseParticle(Particle p)
        {
            p.Return();
            m_ActiveParticles.Remove(p);
            m_Pool.Add(p);
        }

        private Particle ActivateParticle()
        {
            int pop = m_Pool.Count - 1;
            Particle p = m_Pool[pop];
            m_Pool.RemoveAt(pop);
            m_ActiveParticles.Add(p);

            p.Setup(Scale);

            return p;
        }
    }
}