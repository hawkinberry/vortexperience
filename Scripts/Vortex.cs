using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Vortex : MonoBehaviour
    {
        public static VortexEventHandler OnStartShrink;
        public static VortexEventHandler OnStartIntensify;

        private AudioSource m_audioSource;
        [SerializeField] List<AudioClip> absorbSounds;
        [SerializeField] List<AudioClip> intensifySounds;

        [Header("Effects")]
        [SerializeField] private bool m_AddCameraShake;
        public Utility.CameraShake m_Camera;
        [SerializeField] private AudioClip m_lastSound;

        [SerializeField] private List<SpriteRenderer> m_spriteSheet;

        [SerializeField] private float m_minSize = 0.05f;
        [SerializeField] private float m_maxSize = 3.0f;
        [SerializeField] private float m_spinRate = 0.5f;

        private bool m_Started;
        private bool m_Intensify;

        private int shrinkStage;
        private const float TIME_TO_DIE = 0.5f;//1.2f;

        [Header("Settings")]
        private float m_shrinkRate = 0.1f; // how fast (per sec) the scale shrinks
        private float m_massFactor = 0.5f; // factor by which object mass contributes to scale
        private float m_instability = 0.02f; // adds to shrink rate every time you throw into it

        private Vacuum m_Vacuum;
        [SerializeField] private ParticleAttractor m_particles;

        private float Size
        {
            get { return transform.localScale.x; }
        }

        private void OnEnable()
        {
            OnStartShrink += StartShrinking;
            OnStartIntensify += StartIntensifying;
        }

        private void OnDisble()
        {
            OnStartShrink -= StartShrinking;
            OnStartIntensify -= StartIntensifying;
        }

        private void StartShrinking()
        {
            m_Started = true;
        }

        private void StartIntensifying()
        {
            m_Intensify = true;
        }

        private void Awake()
        {
            m_Vacuum = transform.GetComponentInChildren<Vacuum>();
        }

        private void Start()
        {
            m_shrinkRate = Game.Settings.ShrinkStages[shrinkStage++];
            m_massFactor = Game.Settings.VortexMassFactor;
            m_instability = Game.Settings.VortexInstability;

            if (m_audioSource = GetComponent<AudioSource>())
            {
                Utility.Audio.AudioManager.RegisterSFXSource(ref m_audioSource);
                m_audioSource.volume = Utility.Audio.AudioManager.VOLUME_SFX;
            }
        }


        // Update is called once per frame
        void Update()
        {
            if (m_Started)
            {
                Add(-m_shrinkRate * Time.deltaTime);
                //m_particles.Scale = transform.localScale.x;
            }

            Rotate(m_spinRate);

            if (Size <= m_minSize)
            {
                IEnumerator coroutine = WaitAndDie(TIME_TO_DIE);
                StartCoroutine(coroutine);

                m_audioSource.pitch += Time.deltaTime;
                m_shrinkRate = 0.2f;
            }
        }

        private IEnumerator WaitAndDie(float waitTime)
        {
            m_Vacuum.IsActive = false;
            yield return new WaitForSeconds(waitTime);
            SelfDestruct();
        }

        private void PlayAbsorbSound()
        {
            AudioClip played = Utility.Audio.AudioManager.PlayRandomSound(ref absorbSounds, Utility.Audio.AudioManager.VOLUME_SFX);
        }

        private void PlayIntensifySound()
        {
            AudioClip played = Utility.Audio.AudioManager.PlayRandomSound(ref intensifySounds, Utility.Audio.AudioManager.VOLUME_SFX * 0.5f);
        }

        private void Add(float amount)
        {
            // If the operation does not push the size outside acceptable bounds
            //if ((Size >= m_minSize) && (Size <= m_maxSize)) // allow to go one step above so we can check for lose condition
            if ((Size >= 0) && (Size + amount <= m_maxSize)) // don't shrink below zero
            {
                transform.localScale = new Vector3(transform.localScale.x + amount, transform.localScale.y + amount, transform.localScale.z);
            }
        }

        private void Rotate(float amount)
        {
            int idx = 1;
            foreach (SpriteRenderer sr in m_spriteSheet)
            {
                sr.transform.Rotate(Vector3.forward, amount * Time.deltaTime * idx);
                idx += 3;
            }
        }

        private void Intensify(float amount)
        {
            m_shrinkRate += amount;

            if (shrinkStage < Game.Settings.ShrinkStages.Count &&
                m_shrinkRate >= Game.Settings.ShrinkStages[shrinkStage])
            {
                AdvanceStage();
            }
        }

        private void AdvanceStage()
        {
            PlayIntensifySound();

            Debug.Log("Reached stage " + shrinkStage);
            shrinkStage++;

            if (m_AddCameraShake) StartCoroutine(m_Camera.Shake(0.5f, .05f));

            Game.OnGameIntensify?.Invoke();
        }

        private void Absorb(Projectile p)
        {
            m_Vacuum.StopSound();
            PlayAbsorbSound();

            Add(p.Mass * m_massFactor);

            if (m_Intensify)
            {
                Intensify(m_instability);
            }

            if (m_AddCameraShake) StartCoroutine(m_Camera.Shake(0.01f, .005f));

            Destroy(p.gameObject); // would be smarter to pool and reuse objects, but not in this version

            BlobSpawner.BlobDestroyed?.Invoke();
        }

        private void SelfDestruct()
        {
            // ---- END GAME !!!
            Utility.Executive.OnGameOver?.Invoke();

            Utility.Audio.AudioManager.PlaySfx(m_lastSound);
            Destroy(gameObject);

            // Environment responses
            m_particles.Disable();
            Projectile.OnRestoreGravity?.Invoke();
        }

        // Star has dynamic rigid body, which detects collisions with kinematic bodies
        // Destroy kinematic bodies on collision
        void OnTriggerEnter2D(Collider2D c)
        {
            Projectile collider = c.gameObject.GetComponent<Projectile>();

            if (collider)
            {
                Absorb(collider);

                // ------ TUTORIAL ----
                if (Tutorial.InProgress())
                {
                    BlobSpawner.RequestSpawn?.Invoke(false);
                    Tutorial.OnPlayerHitVortex?.Invoke();
                } // ---- TUTORIAL ----

            }
        }

        public delegate void VortexEventHandler();
    }
}