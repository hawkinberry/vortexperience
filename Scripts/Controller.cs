using UnityEngine;
using UnityEngine.EventSystems;

namespace Vortex
{
    public class Controller : Utility.Actor.Controller
    {
        private Player m_Player;

        public bool m_Debug;

        // TUTORIAL
        private bool m_FirstMove = false;
        private bool m_FirstInteract = true;
        //public string HELP_TEXT = "Press SPACE to interact.";

        [Header("Throwing")]
        private float m_basePower = 8.0f;
        private float m_maxPower = 15.0f;
        private float m_pullRate = 0.02f;
        [SerializeField] private float m_cancelTimeoutSec = 0.2f;
        private float timeSinceCancel = 0.0f;
        private bool CancelTimedOut
        {
            get { return timeSinceCancel >= m_cancelTimeoutSec; }
        }

        private float pullback
        {
            get { return m_Player.Pullback; }
            set { m_Player.Pullback = value; }
        }

        private const int LEFT_MOUSE_BUTTON = 0;
        private const int RIGHT_MOUSE_BUTTON = 1;

        public override void Reset()
        {
            // do nothing
        }

        private void Awake()
        {
            m_Player = GetComponent<Player>();

            m_basePower = Game.Settings.PlayerBasePower;
            m_maxPower = Game.Settings.PlayerMaxPower;
            m_pullRate = Game.Settings.PlayerDrawRate;
        }

        private void HandleInput()
        {
            // Right Click
            if (Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON))
            {
                if (m_Debug) { Debug.Log("Right mouse button down"); }

                CancelThrow();

                return;
            }

            // Primary Click
            if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
            {
                if (m_Debug) { Debug.Log("Left mouse button down"); }

                ChargeThrow();

                // If we are over a UI element
                // This is an oddly named function, but it seems to do the trick
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    // Seems to return true for colliders as well. May need to work with layers
                    //return;
                }

            }
            else if (Input.GetMouseButtonUp(LEFT_MOUSE_BUTTON))
            {
                if (m_Debug) { Debug.Log("Left mouse button up"); }

                ReleaseThrow();
            }
        }

        // Update is called once per frame
        void Update()
        {
            timeSinceCancel += Time.deltaTime;

            HandleInput();

            Vector2 movement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            if (!m_FirstMove && movement != Vector2.zero)
            {
                m_FirstMove = true;
                Utility.Executive.OnGameStart?.Invoke();
            }

            m_Player.Move(movement);
        }

        private void ChargeThrow()
        {
            if (CancelTimedOut)
            {
                if (m_FirstInteract) m_FirstInteract = false;
                // Don't want to limit to first time in case player hits the cue early
                // Maybe a performance cost invoking this every time
                // ------ TUTORIAL ----
                if (Tutorial.InProgress())
                {
                    Tutorial.OnPlayerRaiseHand?.Invoke();
                } // ---- TUTORIAL ----

                // Add pullRate to pullBack strength as player holds mouse button
                // Clamp to not exceed max power when added to base power
                float maxPull = m_maxPower - m_basePower;
                pullback = Mathf.Clamp(pullback + (m_pullRate * Time.deltaTime), 0.0f, maxPull);
                m_Player.SetCharge(pullback / maxPull);
            }
        }

        private void ReleaseThrow()
        {
            if (CancelTimedOut)
            {
                Vector3 position = ScreenToWorldPoint(Input.mousePosition);
                Vector2 direction = (position - transform.position).normalized;

                // Throw in direction of mouse click with reserved pullBack power
                float throwStrength = m_basePower + pullback;
                //Debug.Log("Throwing with strength " + throwStrength + " (pullback " + pullBack + ")");
                m_Player.Throw(direction * throwStrength);
                // Reset pullback
                pullback = 0.0f;
            }
        }

        private void CancelThrow()
        {
            pullback = 0.0f;
            timeSinceCancel = 0.0f;

            m_Player.Cancel();
        }

        private Vector3 GetCurrentMousePosition()
        {
            Vector3 clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            return new Vector3(clickPoint.x, clickPoint.y, 0);
        }

        public Vector2 ScreenToWorldPoint(Vector2 screenPosition)
        {
            Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);

            return worldPosition;
        }
    }
}