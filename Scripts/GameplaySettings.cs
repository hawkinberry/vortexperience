using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    // Use this set of fields to balance the game
    [CreateAssetMenu(menuName = "Scriptable Objects/Gameplay Settings")]
    public class GameplaySettings : ScriptableObject
    {
        [Header("Vortex")]
        public float VortexShrinkRate = 0.01f;
        public float VortexMassFactor = 0.2f;
        public float VortexInstability = 0.002f;
        public List<float> ShrinkStages;

        [Header("Player")]
        public float PlayerBasePower = 8.0f;
        public float PlayerMaxPower = 23.0f;
        public float PlayerDrawRate = 8.0f;

        [Header("Spawner")]
        public List<float> SpawnRates;
        public int GlobalLimit = 30;
    }
}
