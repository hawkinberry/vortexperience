using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class PickupObjects : MonoBehaviour
    {
        private Player m_Player;

        private void Awake()
        {
            m_Player = GetComponentInParent<Player>();
        }

        void OnTriggerEnter2D(Collider2D c)
        {
            Projectile collider = c.gameObject.GetComponent<Projectile>();

            if (collider)
            {
                if (m_Player)
                {
                    m_Player.HoldObject(collider);
                }

                // ------ TUTORIAL ----
                if (Tutorial.InProgress())
                {
                    Tutorial.OnPlayerRetrievedBlob?.Invoke();
                } // ---- TUTORIAL ----
            }
        }
    }
}