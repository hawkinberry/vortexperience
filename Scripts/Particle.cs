using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Collider2D))]
    public class Particle : MonoBehaviour
    {
        private Rigidbody2D m_rigidBody;
        public Rigidbody2D Rigidbody { get { return m_rigidBody; } }

        private SpriteRenderer m_spriteRenderer;
        public SpriteRenderer SpriteRenderer { get { return m_spriteRenderer; } }

        private Collider2D m_collider;
        public Collider2D Collider { get { return m_collider; } }

        public Color startColor;
        public Color endColor;
        private float lifetime;
        private float lifetimeScale;
        private bool die = false;

        // Start is called before the first frame update
        void Start()
        {
            m_rigidBody = transform.GetComponent<Rigidbody2D>();
            m_spriteRenderer = transform.GetComponent<SpriteRenderer>();
            m_collider = transform.GetComponent<Collider2D>();

            lifetime = 0;
        }

        private void Update()
        {
            if (die)
            {
                lifetime += Time.deltaTime / 5f;
                m_spriteRenderer.color = Color.Lerp(startColor, endColor, lifetime);
            }
        }

        public void AddForce(Vector2 force)
        {
            m_rigidBody.velocity += force * Time.fixedDeltaTime;
        }

        public void Setup(float ltScale)
        {
            gameObject.SetActive(true);

            if (m_spriteRenderer)
            {
                m_spriteRenderer.color = startColor;
                lifetime = 0;
                lifetimeScale = ltScale;
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Return()
        {
            Hide();
            m_rigidBody.velocity = Vector2.zero;
            m_spriteRenderer.color = startColor;
        }

        public void Inert()
        {
            lifetime = 0;
            die = true;
        }
    }
}