using UnityEngine;
using UnityEngine.EventSystems;

namespace Vortex
{
    public class SelectCard : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        private Animator m_Animator;

        private const string HOVER_BOOL = "m_Hover";

        public GameObject m_PlayerPrefab;

        public static string PLAYER_PREFERENCE;

        private void Start()
        {
            m_Animator = GetComponentInChildren<Animator>();
            PLAYER_PREFERENCE = "PlayerPrefab";
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            m_Animator.SetBool(HOVER_BOOL, true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            m_Animator.SetBool(HOVER_BOOL, false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("Picked " + name);
            PlayerPrefs.SetString(PLAYER_PREFERENCE, m_PlayerPrefab.name);
            Utility.SceneNavigation.GetInstance().OnStartGame();
        }
    }
}