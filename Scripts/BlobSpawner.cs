using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class BlobSpawner : MonoBehaviour
    {
        public static BlobSpawnerTriggerHandler RequestSpawn;
        public static BlobSpawnerHandler EnableAutoSpawn;
        public static BlobSpawnerHandler EnableBreak;
        public static BlobSpawnerHandler BlobDestroyed;

        public bool m_Debug = false;

        [SerializeField] private Projectile projectile;

        [SerializeField] private AudioClip dispenseSound;
        
        private AudioSource m_audioSource;
        private ParticleSystem m_Smoke;

        private bool m_isEnabled;

        private float m_spawnTimeout = 5.0f;
        private float timeSinceSpawn = Mathf.Infinity;
        private bool isActive = false;
        private int spawnStage = 0;
        
        private int m_globalLimit;
        private int currentCount = 0;

        public static float BREAK_SPACE_TIME = 20.0f;
        private bool isBreakable = false;
        private bool isBroken = false;
        private float timeSinceCheckBreak = 0f;
        private float chanceToBreak = 0f;
        private const float m_chanceIncrement = 0.01f;
        private const float m_checkTimeout = 0.5f; // check breakage every x seconds
        [SerializeField] private float m_breakThreshold = 0.2f; // minimum threshold so we don't break too early on a fluke
        [SerializeField] private AudioClip m_brokenSound;
        [SerializeField] private AudioClip m_smokeSound;
        public List<AudioClip> m_clangSounds;

        [SerializeField] private float m_spread;
        [SerializeField] private float m_spreadTick;

        private bool isTimeToSpawn { get { return timeSinceSpawn >= m_spawnTimeout; } }

        void OnEnable()
        {
            RequestSpawn += Spawn;
            EnableAutoSpawn += EnableSpawnClock;
            EnableBreak += EnableBreakClock;
            BlobDestroyed += Subtract;
            Game.OnGameIntensify += Intensify;
            Utility.Executive.OnEndGame += Disable;
        }

        void OnDisable()
        {
            RequestSpawn -= Spawn;
            EnableAutoSpawn -= EnableSpawnClock;
            EnableBreak -= EnableBreakClock;
            BlobDestroyed -= Subtract;
            Game.OnGameIntensify -= Intensify;
            Utility.Executive.OnEndGame -= Disable;
        }

        private bool WaitLonger
        {
            get { return !isTimeToSpawn && isActive; }
        }

        private bool SpareRoom
        {
            get { return currentCount < m_globalLimit; }
        }

        private void Start()
        {
            m_spawnTimeout = Game.Settings.SpawnRates[spawnStage];
            m_globalLimit = Game.Settings.GlobalLimit;

            if (m_audioSource = GetComponent<AudioSource>())
            {
                Utility.Audio.AudioManager.RegisterSFXSource(ref m_audioSource);
            }

            m_Smoke = GetComponent<ParticleSystem>();
            chanceToBreak = m_breakThreshold; // start off by guaranteeing a breakage fairly early
        }

        void Update()
        {
            if (isActive)
            {
                timeSinceSpawn += Time.deltaTime;

                if (isTimeToSpawn && !isBroken)
                {
                    Spawn();
                }

                if (isBreakable && !isBroken)
                {
                    timeSinceCheckBreak += Time.deltaTime;

                    // only check at m_checkTimeout interval
                    // otherwise we are checking every Update and we lose that probability gamble
                    if (timeSinceCheckBreak >= m_checkTimeout)
                    {
                        timeSinceCheckBreak = 0f;
                        float randomDraw = Random.Range(m_breakThreshold, 1f);

                        if (randomDraw < chanceToBreak)
                        {
                            if (m_Debug) { Debug.Log("Random draw = " + randomDraw + ", Chance to break = " + chanceToBreak); }
                            Break();
                        }
                    }
                    else
                    {
                        chanceToBreak += m_chanceIncrement * Time.deltaTime;
                    }
                }
            }
        }

        public void EnableSpawnClock()
        {
            isActive = true;
        }

        public void EnableBreakClock()
        {
            isBreakable = true;
        }

        public void Disable()
        {
            Debug.Log("Disabling spawner!");
            isActive = false;
        }

        private void Break()
        {
            chanceToBreak = m_breakThreshold;
            m_Smoke.Play();

            Debug.Log("Spawner broken!");
            m_audioSource.PlayOneShot(m_brokenSound, Utility.Audio.AudioManager.VOLUME_SFX);
            m_audioSource.PlayOneShot(m_smokeSound, Utility.Audio.AudioManager.VOLUME_SFX);

            Broken.OnPipeBroken?.Invoke();

            // Spawn a bunch quickly just so the player isn't left without a fix
            Invoke("Burst", 0.0f);
            Invoke("Burst", 0.03f);
            Invoke("Burst", 0.09f);

            isBroken = true;
            isBreakable = false;
        }

        private void Fix()
        {
            Debug.Log("Spawner fixed!");
            isBroken = false;
            m_Smoke.Stop();
            m_audioSource.Stop();

            Broken.OnPipeFixed?.Invoke();
        }

        void Burst()
        {
            Spawn(true);
        }

        // burst allows spawner to bypass timing requirement
        void Spawn(bool burst = false)
        {
            if (!burst && (WaitLonger || !SpareRoom))
            {
                return;
            }

            GameObject obj = GameObject.Instantiate(projectile.gameObject, transform.Find("Spawn Point").transform.position, Quaternion.identity, transform.parent);

            PlayDispenseSound();

            // Give velocity
            Vector3 velocity = new Vector3(Random.Range(-1.0f, 1.0f) * m_spread, -2.5f, 0f);
            obj.GetComponent<Projectile>().Launch(velocity);

            currentCount++;
            timeSinceSpawn = 0.0f;

            if (m_Debug) { Debug.Log("Current blob count " + currentCount); }
        }

        void Subtract()
        {
            currentCount = Mathf.Clamp(currentCount - 1, 0, m_globalLimit);
            if (m_Debug) { Debug.Log("Current blob count " + currentCount); }
        }

        void Intensify()
        {
            // increase spawn rate
            if (spawnStage + 1 < Game.Settings.SpawnRates.Count)
            {
                m_spawnTimeout = Game.Settings.SpawnRates[++spawnStage];
            }

            // increase spawn spread
            m_spread += m_spreadTick;

            // after we reach the first stage, set to breakable
            //isBreakable = true;
        }

        void PlayDispenseSound()
        {
            if (dispenseSound) m_audioSource.PlayOneShot(dispenseSound, Utility.Audio.AudioManager.VOLUME_SFX * 0.5f);
        }

        private void OnCollisionEnter2D(Collision2D c)
        {
            Projectile collider = c.collider.gameObject.GetComponent<Projectile>();

            if (collider)
            {
                Utility.Audio.AudioManager.PlayRandomSound(ref m_clangSounds, Utility.Audio.AudioManager.VOLUME_SFX);

                if (isBroken)
                {
                    Fix();
                }
            }
        }

        // Game Delegates
        public delegate void BlobSpawnerHandler();
        public delegate void BlobSpawnerTriggerHandler(bool value);
    }
}