using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    using Utility.Audio;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class Character : MonoBehaviour
    {
        public static string PLAYER_ANIMATION_THROW_TRIGGER = "m_Throw";
        public static string PLAYER_ANIMATION_CANCEL_TRIGGER = "m_Cancel";
        public static string PLAYER_ANIMATION_MOVEMENT = "m_Speed";
        public static string PLAYER_ANIMATION_PULL = "m_Pullback";

        [Header("Sprite")]
        public bool m_FacingRight = true;

        [Header("Movement")]
        [SerializeField] protected float m_MovementSpeed;
        //[SerializeField] protected Vector2 m_Direction;
        // How much to smooth out diagonal movement
        [Range(0.2f, 1f)] [SerializeField] protected float m_MovementSmoothing = .7f;
        protected Vector2 currentVelocity;

        protected Rigidbody2D m_Body;
        protected Collider2D m_Collider;
        protected Animator m_Animator;
        protected SpriteRenderer m_Sprite;

        // Start is called before the first frame update
        protected virtual void Start()
        {
            m_Body = GetComponent<Rigidbody2D>();
            m_Collider = GetComponent<Collider2D>();
            m_Sprite = GetComponentInChildren<SpriteRenderer>();
            m_Animator = GetComponentInChildren<Animator>();
        }

        protected virtual void Update()
        {
            if ((currentVelocity.x > 0 && !m_FacingRight) ||
                    (currentVelocity.x < 0 && m_FacingRight))
            {
                Flip();
            }
        }

        protected void CalculateVelocity(Vector2 direction)
        {
            currentVelocity = direction * m_MovementSpeed * Time.fixedDeltaTime;

            // slow down diagonal movement to make it consistent
            if (Mathf.Abs(currentVelocity.x) > 0 && Mathf.Abs(currentVelocity.y) > 0)
            {
                currentVelocity *= m_MovementSmoothing;
            }

            if (m_Animator)
            {
                m_Animator.SetFloat(PLAYER_ANIMATION_MOVEMENT, Mathf.Abs(currentVelocity.magnitude));
            }
        }

        protected void DoMove(Vector2 velocity)
        {
            m_Body.MovePosition((Vector2)(transform.position) + velocity);
        }

        public virtual void Move(Vector2 direction)
        {
            CalculateVelocity(direction);
            DoMove(currentVelocity);
        }

        // Only flips the sprite, not the object
        protected virtual void Flip()
        {
            m_FacingRight = !m_FacingRight;

            Vector3 scale = m_Sprite.transform.localScale;
            scale.x *= -1;
            m_Sprite.transform.localScale = scale;
        }

        protected virtual void Disable()
        {
            m_Animator.SetFloat(PLAYER_ANIMATION_MOVEMENT, 0f);
        }

    }
}
