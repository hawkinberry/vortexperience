using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Vortex
{
    public class Game : MonoBehaviour
    {
        public static GameEventHandler OnGameStart;
        public static GameEventHandler OnGameIntensify;

        private static Game m_Instance;

        [SerializeField] private GameplaySettings m_Settings;
        public static GameplaySettings Settings
        {
            get { return GetInstance().m_Settings; }
        }

        public TextMeshProUGUI oldDisplay;
        public TextMeshProUGUI timeScoreboard;
        public TextMeshProUGUI timer;
        private float timeScore;

        private bool m_startClock;

        [Header("End Bits")]
        public Animator m_RunAtEnd;

        [Header("Player Options")]
        [SerializeField] private List<GameObject> m_playerPrefabs;

        private void OnEnable()
        {
            OnGameStart += StartGame;
            Utility.Executive.OnGameOver += EndGame;
            OnGameIntensify += Intensify;
        }

        private void OnDisable()
        {
            OnGameStart -= StartGame;
            Utility.Executive.OnGameOver -= EndGame;
            OnGameIntensify -= Intensify;
        }

        public static Game GetInstance()
        {
            return m_Instance;
        }

        private void Awake()
        {
            if (!m_Instance)
            {
                m_Instance = this;
            }

            m_startClock = false;
            GameObject selectedPlayer = m_playerPrefabs[0];

            if (PlayerPrefs.HasKey(SelectCard.PLAYER_PREFERENCE))
            {
                GameObject result = m_playerPrefabs.Find(x => x.name == PlayerPrefs.GetString(SelectCard.PLAYER_PREFERENCE));

                if (result)
                {
                    selectedPlayer = result;
                }
            }

            Vector3 spawnLocation = transform.Find("Spawn Location").position;
            Instantiate(selectedPlayer, spawnLocation, Quaternion.identity, transform.parent);
        }

        private void Update()
        {
            timeScore += Time.deltaTime;

            if (m_startClock) timer.text = timeScore.ToString("0.00");
        }

        private void Intensify()
        {
            // ------ PROGRESS ----
            if (Progress.InProgress())
            {
                Progress.OnStageIntensify?.Invoke();
            } // ---- PROGRESS ----
        }

        private void StartGame()
        {
            timeScore = 0f;
            m_startClock = true;
        }

        private void EndGame()
        {
            Utility.Executive.OnEndGame?.Invoke();

            timer.transform.parent.gameObject.SetActive(false);
            oldDisplay.text = "";
            timeScoreboard.text = timeScore.ToString("0.00");
        }

        public delegate void GameEventHandler();
    }
}