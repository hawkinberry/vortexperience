using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Vortex
{
    public class ScriptExecutive : MonoBehaviour
    {
        public bool m_Debug;
        private static ScriptExecutive m_Instance;
        public static ScriptExecutive GetInstance()
        {
            return m_Instance;
        }

        public ScriptPlayer scriptPlayer;

        private List<ScriptManager> m_Scripts;
        [HideInInspector] public static List<ScriptManager> Scripts
        {
            get { return m_Instance.m_Scripts; }
        }

        [Header("Sound Controls")]
        [SerializeField] private GameObject m_NarratorOnButton;
        [SerializeField] private GameObject m_NarratorOffButton;

        [Header("Help Text")]
        [SerializeField] private TextMeshProUGUI m_helpBox;
        [SerializeField] private string m_introductionSkip = "Press [space] to skip introduction";
        [SerializeField] private string m_tutorialHelpString = "Press [space] to skip tutorial";
        [SerializeField] private string m_pauseHelpString = "[P] to pause";

        [Header("Scripts")]
        [SerializeField] private ScriptManager m_ScriptIntroduction; // Should be a sequential advance
        [SerializeField] private ScriptManager m_ScriptTutorial; // Should be a sequential advance, with some waiting for events
        [SerializeField] private ScriptManager m_ScriptProgress; // Should be a sequential advance triggered by story events
        [SerializeField] private ScriptManager m_ScriptBanter; // Should be a random pull after enough time of silence
        //[SerializeField] private ScriptManager m_Script_Reactions; // Specific banter triggered by events -- HARDER
        [SerializeField] private ScriptManager m_ScriptPipe; // should be sequential advance triggered by story events
        [SerializeField] private ScriptManager m_Script_End_Loss; // Sequence of lines + additional banter ?
        //[SerializeField] private ScriptManager m_Script_End_Win; // Is there even a "win" condition?

        private bool m_tutorialComplete;

        private bool isPlaying
        {
            get { return scriptPlayer.IsPlaying; }
        }

        private void OnEnable()
        {
            Utility.Executive.OnGameOver += EndGame;
        }

        private void OnDisable()
        {
            Utility.Executive.OnGameOver -= EndGame;
        }

        // Start is called before the first frame update
        void Awake()
        {
            m_tutorialComplete = false;

            if (!m_Instance)
            {
                m_Instance = this;
            }

            m_Scripts = new List<ScriptManager>();
            if (m_ScriptIntroduction) m_Scripts.Add(m_ScriptIntroduction);
            if (m_ScriptTutorial) m_Scripts.Add(m_ScriptTutorial);
            if (m_ScriptProgress) m_Scripts.Add(m_ScriptProgress);
            if (m_ScriptBanter) m_Scripts.Add(m_ScriptBanter);
            if (m_ScriptPipe) m_Scripts.Add(m_ScriptPipe);
            if (m_Script_End_Loss) m_Scripts.Add(m_Script_End_Loss);

            foreach (ScriptManager s in m_Scripts)
            {
                s?.Init();
            }

            if (m_ScriptTutorial)
            {
                StartTutorial();
            }
        }

        private void Start()
        {
            if (m_NarratorOffButton) m_NarratorOffButton.SetActive(true);
            if (m_NarratorOnButton)  m_NarratorOnButton.SetActive(false);
        }


        void Update()
        {
            foreach (ScriptManager s in m_Scripts)
            {
                if (m_Debug) { Debug.Log(s.name + " is " + (s.IsComplete ? "" : "not") + " complete"); }
                if (!(s.IsComplete))
                {
                    s?.CheckStatus();
                }
            }

            if (m_ScriptIntroduction)
            {
                // If we just finished and aren't waiting for the last line to finish
                if ((m_ScriptIntroduction.IsComplete) && !scriptPlayer.IsPlaying)
                {
                    EndIntroduction();
                }
            }

            if (m_ScriptTutorial)
            {
                // If we just finished and aren't waiting for the last line to finish
                if ((!m_tutorialComplete && m_ScriptTutorial.IsComplete) && !scriptPlayer.IsPlaying)
                {
                    EndTutorial();
                }
            }
        }

        public void StartIntroduction()
        {
            Utility.Executive.OnGameStart?.Invoke();
            if (m_helpBox) m_helpBox.text = m_introductionSkip;
            m_ScriptIntroduction?.Play();
        }

        private void EndIntroduction()
        {
            if (m_helpBox) m_helpBox.text = "";
            scriptPlayer.Stop();
        }

        private void StartTutorial()
        {
            if (m_helpBox) m_helpBox.text = m_tutorialHelpString;
            m_ScriptTutorial?.Play();
        }

        private void EndTutorial()
        {
            m_tutorialComplete = true;

            Game.OnGameStart?.Invoke();

            m_ScriptTutorial.End();

            scriptPlayer.Stop();

            StartProgress();
        }

        private void StartProgress()
        {
            m_helpBox.text = m_pauseHelpString;
            if (!m_ScriptProgress.IsStarted) m_ScriptProgress.Play();
            if (!m_ScriptBanter.IsStarted) m_ScriptBanter.Play();
            if (!m_ScriptPipe.IsStarted) m_ScriptPipe.Play();
        }
        
        private void EndGame()
        {
            m_helpBox.text = "";

            m_ScriptProgress.End();
            m_ScriptBanter.End();
            m_ScriptPipe.End();

            scriptPlayer.TurnOff();

            if (!m_Script_End_Loss.IsStarted) m_Script_End_Loss.Play();
        }

        // Returns ScriptPlayer.NOTHING_TO_PLAY (-1) if cannot be played
        public static float RequestPlayLine(LineFile line)
        {
            ScriptExecutive me = GetInstance();

            float wait = me.scriptPlayer.PlayLine(line);

            return wait;
        }

        public void ToggleNarratorAudio()
        {
            bool muted = scriptPlayer.ToggleMute();

            m_NarratorOffButton.SetActive(!muted);
            m_NarratorOnButton.SetActive(muted);
        }
    }
}