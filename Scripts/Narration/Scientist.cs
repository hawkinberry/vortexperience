using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Scientist : MonoBehaviour
    {
        public static ScientistAnimationTimeoutEvent OnTalkMic_Into;
        public static ScientistAnimationTimeoutEvent OnTalkMic_Away;
        public static ScientistAnimationTriggerEvent OnLeave;

        Animator m_animator;

        private const string LEAVE_TRIGGER = "m_leave";
        private const string TALKING_TRIGGER = "m_talkingIntoMic";
        private const string ASIDE_TRIGGER = "m_talkingAwayMic";

        private bool isLeaving = false;

        private void Awake()
        {
            m_animator = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            OnTalkMic_Into += TalkIntoMic;
            OnTalkMic_Away += TalkAwayMic;
            OnLeave += ScientistLeave;
        }

        private void OnDisable()
        {
            OnTalkMic_Into -= TalkIntoMic;
            OnTalkMic_Away -= TalkAwayMic;
            OnLeave -= ScientistLeave;
        }

        private void TalkIntoMic(float timeout)
        {
            if (timeout <= 0)
            {
                Debug.LogWarning("Cannot play TalkInto animation for non-positive seconds.");
                return;
            }

            if (!isLeaving)
            {
                m_animator.SetBool(TALKING_TRIGGER, true);

                StartCoroutine(WaitAndStop(timeout, TALKING_TRIGGER));
            }
        }

        private void TalkAwayMic(float timeout)
        {
            if (timeout <= 0)
            {
                Debug.LogWarning("Cannot play TalkAway animation for non-positive seconds.");
                return;
            }

            if (!isLeaving)
            {
                m_animator.SetBool(ASIDE_TRIGGER, true);

                StartCoroutine(WaitAndStop(timeout, ASIDE_TRIGGER));
            }
        }

        private IEnumerator WaitAndStop(float waitTime, string parameter)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                m_animator.SetBool(parameter, false);
                yield break;
            }
        }

        private void ScientistLeave()
        {
            isLeaving = true;
            m_animator.SetTrigger(LEAVE_TRIGGER);
        }

        public delegate void ScientistAnimationTriggerEvent();
        public delegate void ScientistAnimationTimeoutEvent(float timeout);
    }
}