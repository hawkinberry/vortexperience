using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    [CreateAssetMenu(menuName = "Scriptable Objects/ScriptFile")]
    public class ScriptFile : ScriptableObject
    {
        public List<LineFile> Lines;

        public static int RESET = -1;
        private int currentPlace;
        public int CurrentNumber
        {
            get { return currentPlace; }
        }

        private bool complete;
        public bool IsComplete
        {
            // NOTE: If the script has only one line,
            // this breaks and the script IsComplete before it ever plays!
            get { return complete || (currentPlace >= (Lines.Count - 1)); }
        }

        public void Init()
        {
            Reset();
        }

        public void Reset()
        {
            currentPlace = RESET;
            complete = false;
        }

        public void End()
        {
            complete = true;
        }

        public void Shuffle(int begin = 0, int end = 0)
        {
            int n = Lines.Count - end;
            while (n > (1 + begin))
            {
                n--;
                int k = Random.Range(begin, n-1);
                LineFile value = Lines[k];
                Lines[k] = Lines[n];
                Lines[n] = value;
            }
        }

        // Returns next line in the script
        public LineFile GetNextLine()
        {
            LineFile nextLine = null;

            if (currentPlace + 1 < Lines.Count)
            {
                nextLine = Lines[currentPlace + 1];
            }

            //Debug.Log("Current line is #" + currentPlace);

            return nextLine;
        }

        // Advances to the next line in the script
        // Up to the caller to check whether GetNextLine returns a line that has met all conditions
        public LineFile PopNextLine()
        {
            LineFile nextLine = GetNextLine();
            currentPlace++;
            return nextLine;
        }

        public void GoBack()
        {
            currentPlace--;
        }
    }
}