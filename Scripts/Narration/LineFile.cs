using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    [CreateAssetMenu(menuName = "Scriptable Objects/LineFile")]
    public class LineFile : ScriptableObject
    {
        public enum AfterPlayEnum
        {
            KEEP = 0,
            CLEAR = 1,
            REVERT = 2
        };

        [SerializeField] private string section;
        public string Section
        {
            get { return section; }
        }

        [SerializeField] private int number;
        public int Number
        {
            get { return number; }
        }

        [SerializeField] private string line;
        public string Line
        {
            get { return line; }
        }

        [SerializeField] private AudioClip record;
        public AudioClip Record
        {
            get { return record; }
        }

        [SerializeField] private bool autoPlay;
        public bool AutoPlay
        {
            get { return autoPlay; }
        }

        [SerializeField] private bool interrupt;
        public bool Interrupt
        {
            get { return interrupt; }
        }

        [SerializeField] private AfterPlayEnum afterPlay;
        public AfterPlayEnum AfterPlay
        {
            get { return afterPlay; }
        }

        [SerializeField] private float delay;
        public float DelayTime
        {
            get { return delay; }
        }

        [SerializeField] private bool offMic;
        public bool IsOffMic
        {
            get { return offMic; }
        }
    }
}