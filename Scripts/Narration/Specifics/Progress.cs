using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Progress : ScriptManager
    {
        public static ProgressEventHandler OnStageIntensify;

        protected static Progress m_Instance;

        public static Progress GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
        }

        public override void Play()
        {
            base.Play();

            // Fire events tutorial fired, in case it was skipped
            Vortex.OnStartShrink?.Invoke();
            BlobSpawner.EnableAutoSpawn?.Invoke();
            if (!Utility.Audio.AudioManager.IsMusicPlaying()) // tmp
            {
                StartMusic();
            }


            Vortex.OnStartIntensify?.Invoke();
        }

        public override void StartMusic()
        {
            Utility.Audio.AudioManager.SetMusicVolume(0.1f);
            base.StartMusic();
        }

        public override bool CheckStatus()
        {
            return base.CheckStatus();
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine()) // tmp
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = false;
                    switch (nextLine.Number)
                    {
                        case 3:
                            PASS = (m_currentStage == 1);
                            break;
                        case 4:
                            PASS = (m_currentStage == 2);
                            break;
                        case 5:
                            PASS = (m_currentStage == 3);
                            break;
                        default:
                            Debug.LogWarning("Unhandled non-auto-play line " + nextLine.name);
                            break;
                    }

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    float delay = 0f;
                    if (playThis.Record)
                    {
                        delay = playThis.Record.length;
                    }

                    // Any other automatic events to kick-off ?
                    switch (playThis.Number)
                    {
                        case 5:
                            InvokeRepeating("EnableBreak", 0f, BlobSpawner.BREAK_SPACE_TIME);
                            break;
                        default:        
                            Invoke("EnableBreak", delay);
                            break;
                    }
                }
            }

            return playThis;
        }

        private int m_currentStage;
        private void AdvanceProgress()
        {
            m_currentStage++;
        }

        private void EnableBreak()
        {
            BlobSpawner.EnableBreak?.Invoke();
        }


        private void OnEnable()
        {
            OnStageIntensify += AdvanceProgress;
        }

        private void OnDisable()
        {
            OnStageIntensify -= AdvanceProgress;
        }

        public delegate void ProgressEventHandler();
    }
}