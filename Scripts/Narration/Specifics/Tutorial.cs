using UnityEngine;

namespace Vortex
{
    public class Tutorial : ScriptManager
    {
        public static TutorialEventHandler OnTutorialComplete;

        public static TutorialEventHandler OnPlayerRaiseHand;
        public static TutorialEventHandler OnPlayerRetrievedBlob;
        public static TutorialEventHandler OnPlayerHitVortex;

        protected static Tutorial m_Instance;

        public static Tutorial GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
        }

        protected override void Update()
        {
            if (Input.GetButtonDown("Submit")) // tmp
            {
                End();
            }

            base.Update();
        }

        public override void Play()
        {
            base.Play();
        }

        public override void StartMusic()
        {
            Utility.Audio.AudioManager.SetMusicVolume(0.1f);
            base.StartMusic();
        }

        public override bool CheckStatus()
        {
            return base.CheckStatus();
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine())
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = false;
                    switch (nextLine.Number)
                    {
                        case 3:
                            PASS = m_playerRaisedHand;
                            break;
                        case 7:
                            Vortex.OnStartShrink?.Invoke();
                            PASS = m_playerRetrievedBlob;
                            break;
                        case 8:
                            PASS = m_playerHitVortex;
                            break;
                        default:
                            Debug.LogWarning("Unhandled non-auto-play line " + nextLine.name);
                            break;
                    }

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    // Any other automatic events to kick-off ?
                    switch (playThis.Number)
                    {
                        case 3:
                            StartMusic();
                            break;
                        case 5:
                            BlobSpawner.RequestSpawn?.Invoke(true);
                            break;
                        case 10:
                            BlobSpawner.EnableAutoSpawn?.Invoke();
                            break;
                        default:
                            break;
                    }
                }
            }

            return playThis;
        }

        // This is where we record events while waiting for specific lines
        private bool m_playerRaisedHand;
        private void PlayerRaisedHand()
        {
            if (myScript.GetNextLine().Number == 3)
            {
                m_playerRaisedHand = true;
                OnPlayerRaiseHand -= PlayerRaisedHand; // stop listening
            }
        }

        private bool m_playerRetrievedBlob;
        private void PlayerRetrievedBlob()
        {
            if (myScript.GetNextLine().Number <= 7)
            {
                m_playerRetrievedBlob = true;
                OnPlayerRetrievedBlob -= PlayerRetrievedBlob;
            }
        }

        private bool m_playerHitVortex;
        private void PlayerHitVortex()
        {
            if (myScript.GetNextLine().Number == 8)
            {
                m_playerHitVortex = true;
                OnPlayerHitVortex -= PlayerHitVortex;
            }
        }


        private void OnEnable()
        {
            OnPlayerRaiseHand += PlayerRaisedHand;
            OnPlayerRetrievedBlob += PlayerRetrievedBlob;
            OnPlayerHitVortex += PlayerHitVortex;
        }

        private void OnDisable()
        {
            OnPlayerRaiseHand -= PlayerRaisedHand;
            OnPlayerRetrievedBlob -= PlayerRetrievedBlob;
            OnPlayerHitVortex -= PlayerHitVortex;
        }

        public delegate void TutorialEventHandler();
    }
}