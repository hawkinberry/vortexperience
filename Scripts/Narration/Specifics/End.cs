using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class End : ScriptManager
    {
        [SerializeField] private AudioClip m_BackgroundNoise;

        protected static End m_Instance;

        public static End GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
        }

        public override void Play()
        {
            base.Play();
            Utility.Audio.AudioManager.StopMusic();
        }

        public override bool CheckStatus()
        {
            return base.CheckStatus();
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine()) // tmp
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = false;
                    switch (nextLine.Number)
                    {
                        default:
                            Debug.LogWarning("Unhandled non-auto-play line " + nextLine.name);
                            break;
                    }

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    // Any other automatic events to kick-off ?
                    // BUG - if the line file has a delay, we won't get here !!!!
                    switch (playThis.Number)
                    {
                        case 1:
                            Utility.Audio.AudioManager.PlaySfx(m_BackgroundNoise, true);
                            break;
                        case 5:
                            Debug.Log("Scientist leaving");
                            Scientist.OnLeave?.Invoke();
                            Assistant.OnWalk?.Invoke();
                            break;
                        default:
                            break;
                    }
                }
            }

            return playThis;
        }
    }
}