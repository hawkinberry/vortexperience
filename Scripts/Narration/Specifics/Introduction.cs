using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Vortex
{
    public class Introduction : ScriptManager
    {
        protected static Introduction m_Instance;

        public Canvas m_CharacterSelector;
        private bool doSkip = false;

        public static Introduction GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
            StartCharacterSelector(false);
        }

        protected override void Update()
        {
            if (Input.GetButtonDown("Submit"))
            {
                End();
            }

            base.Update();
        }

        private void StartCharacterSelector(bool enable)
        {
            //m_CharacterSelector.enabled = enable;
            m_CharacterSelector.gameObject.SetActive(enable);
        }


        public override void End()
        {
            base.End();
            StartCharacterSelector(true);
        }

        public override bool CheckStatus()
        {
            return base.CheckStatus();
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine()) // tmp
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = false;
                    switch (nextLine.Number)
                    {
                        default:
                            Debug.LogWarning("Unhandled non-auto-play line " + nextLine.name);
                            break;
                    }

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    // Any other automatic events to kick-off ?
                    switch (playThis.Number)
                    {
                        case 5:
                            StartCharacterSelector(true);
                            break;
                        default:
                            break;
                    }
                }
            }

            return playThis;
        }
    }
}