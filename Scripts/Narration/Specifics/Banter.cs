using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Banter : ScriptManager
    {
        protected static Banter m_Instance;

        private int m_currentStage = 0;
        private const int m_StartBanter = 2;

        public static Banter GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
            myScript.Shuffle(1, 1);
        }

        public override bool CheckStatus()
        {
            return base.CheckStatus();
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine()) // tmp
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = false;
                    switch (nextLine.Number)
                    {
                        case 1:
                            PASS = m_currentStage >= m_StartBanter;
                            break;
                        default:
                            Debug.LogWarning("Unhandled non-auto-play line " + nextLine.name);
                            break;
                    }

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    // Any other automatic events to kick-off ?
                    switch (playThis.Number)
                    {
                        default:
                            break;
                    }
                }
            }

            return playThis;
        }

        private void StartBanter()
        {
            m_currentStage++;
        }

        private void OnEnable()
        {
            Progress.OnStageIntensify += StartBanter;
        }

        private void OnDisable()
        {
            Progress.OnStageIntensify -= StartBanter;
        }
    }
}