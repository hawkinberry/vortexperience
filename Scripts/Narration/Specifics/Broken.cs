using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Broken : ScriptManager
    {
        public static ScriptEventHandler OnPipeBroken;
        public static ScriptEventHandler OnPipeFixed;

        protected static Broken m_Instance;

        private int m_currentStage;
        private bool isWaitingToPlay;

        public static Broken GetInstance()
        {
            return m_Instance;
        }

        public static bool InProgress()
        {
            return !GetInstance().myScript.IsComplete;
        }

        protected void Awake()
        {
            m_Instance = this;
        }

        public override void Play()
        {
            base.Play();

            if (!Utility.Audio.AudioManager.IsMusicPlaying()) // tmp
            {
                StartMusic();
            }
        }

        public override void StartMusic()
        {
            Utility.Audio.AudioManager.SetMusicVolume(0.1f);
            base.StartMusic();
        }

        public override bool CheckStatus()
        {
            isWaitingToPlay = base.CheckStatus();

            return isWaitingToPlay;
        }

        public override LineFile Advance()
        {
            LineFile nextLine = null;
            LineFile playThis = null;

            // do check to proceed
            if (nextLine = myScript.GetNextLine()) // tmp
            {
                if (nextLine.AutoPlay)
                {
                    playThis = base.Advance();
                }
                // HERE we can do a bunch of if-else states to check for conditions !!
                else
                {
                    bool PASS = (m_currentStage == nextLine.Number);

                    if (PASS)
                    {
                        playThis = base.Advance();
                    }
                }

                if (playThis)
                {
                    // Any other automatic events to kick-off ?
                    switch (playThis.Number)
                    {
                        default:
                            break;
                    }
                }
            }

            return playThis;
        }

        private void AdvanceProgress()
        {
            //m_currentStage = m_savedStage;
            m_currentStage++;
        }

        private void PauseProgress()
        {
            if (isWaitingToPlay)
            {
                m_currentStage--;
                GoBack();
            }
        }

        private void OnEnable()
        {
            OnPipeBroken += AdvanceProgress;
            OnPipeFixed += PauseProgress;
            //OnPipeFixed += AdvanceProgress;
        }

        private void OnDisable()
        {
            OnPipeBroken -= AdvanceProgress;
            OnPipeFixed -= PauseProgress;
            //OnPipeFixed -= AdvanceProgress;
        }

        public delegate void ScriptEventHandler();
    }
}