using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public class Assistant : MonoBehaviour
    {
        public static AssistantAnimationTriggerEvent OnWalk;

        Animator m_animator;

        private const string WALK_TRIGGER = "m_walk";


        private void Awake()
        {
            m_animator = GetComponent<Animator>();
        }

        private void OnEnable()
        {
            OnWalk += AssistantWalk;
        }

        private void OnDisable()
        {
            OnWalk -= AssistantWalk;
        }

       
        private void AssistantWalk()
        {
            m_animator.SetTrigger(WALK_TRIGGER);
        }

        public delegate void AssistantAnimationTriggerEvent();
        public delegate void AssistantAnimationTimeoutEvent(float timeout);
    }
}