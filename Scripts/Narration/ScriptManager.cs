using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    public abstract class ScriptManager : MonoBehaviour
    {
        public bool m_Debug;

        [SerializeField] protected ScriptFile myScript;

        public AudioClip m_musicTrack;

        protected LineFile lineOnDeck;

        private bool m_Started;
        private float countdownClock;
        private float timeElapsed;

        public bool IsStarted
        {
            get { return m_Started; }
        }

        public bool IsComplete
        {
            get { return (myScript && myScript.IsComplete); } // nullcheck first
        }

        public float CountdownClock
        {
            get { return countdownClock; }
            // Setting this to value will automatically add a narration buffer
            set { countdownClock = value + ScriptPlayer.NARRATION_BUFFER_SECONDS; }
        }

        protected bool clockExpired
        {
            get { return countdownClock <= 0; }
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (m_Started)
            {
                countdownClock -= Time.deltaTime;
                timeElapsed += Time.deltaTime;
            }
        }

        public virtual void StartMusic()
        {
            if (m_musicTrack)
            {
                Utility.Audio.AudioManager.PlayMusic(m_musicTrack, true);
            }
        }

        public virtual void Init()
        {
            if (myScript)
            {
                myScript.Init();
            }
        }

        public virtual void Play()
        {
            if (m_Debug) { Debug.Log("Playing " + name); }
            m_Started = true;
        }

        public virtual void End()
        {
            if (m_Debug) { Debug.Log("Ending " + name); }

            m_Started = false;
            myScript.End();
        }

        // Returns waiting to play
        public virtual bool CheckStatus()
        {
            LineFile newLine;
            float timeToWait;

            if (!(m_Started && clockExpired))
            {
                return false;
            }

            //Debug.Log("Time scale is " + Time.timeScale);

            // If we are waiting to play something
            if (lineOnDeck)
            {
                if (timeElapsed >= lineOnDeck.DelayTime && Time.timeScale > 0)
                {
                    // and the Executive plays it
                    //if ((playtime = ScriptExecutive.RequestPlayLine(lineOnDeck)) != ScriptPlayer.CURRENTLY_PLAYING)
                    if ((timeToWait = ScriptExecutive.RequestPlayLine(lineOnDeck)) > ScriptPlayer.CURRENTLY_PLAYING)
                    {
                        if (m_Debug) { Debug.Log("Playing track " + lineOnDeck.name); }
                        ResetDeck(timeToWait);
                    }
                    else
                    {
                        if (m_Debug) { Debug.Log("Waiting for previous line to finish."); }
                        return true;
                    }
                }
            }
            // Or if we are ready to advance
            else if ((newLine = Advance()))
            {
                timeToWait = newLine.DelayTime - timeElapsed;
                // and we don't have to wait
                // and the Executive plays it
                //if ((playtime = ScriptExecutive.RequestPlayLine(newLine)) != ScriptPlayer.CURRENTLY_PLAYING)
                if (timeElapsed >= newLine.DelayTime && Time.timeScale > 0 &&
                    (timeToWait = ScriptExecutive.RequestPlayLine(newLine)) > ScriptPlayer.CURRENTLY_PLAYING)
                {
                    if (m_Debug) { Debug.Log("Playing track " + newLine.name); }
                    ResetDeck(timeToWait);
                }
                else
                {
                    lineOnDeck = newLine;
                    if (m_Debug) { Debug.Log("Waiting " + timeToWait + ". Queuing Line #" + lineOnDeck.name); }
                    return true;
                }
            }

            if (m_Debug)
            {
                if (CountdownClock > 0)
                {
                    Debug.Log("Waiting " + CountdownClock + " to play next file.");
                }
            }

            return false;
        }

        protected void TurnAwayFromMic(float timeout)
        {
            Scientist.OnTalkMic_Away(timeout);
        }

        private void ResetDeck(float timeToWait = 0f, LineFile onDeck = null)
        {
            lineOnDeck = onDeck;
            CountdownClock = timeToWait;
            timeElapsed = 0;
        }

        public virtual LineFile Advance()
        {
            LineFile nextLine;
            if (nextLine = myScript.PopNextLine())
            {
                if (nextLine.DelayTime > 0)
                {
                    ResetDeck(nextLine.DelayTime, nextLine);
                    nextLine = null; // return nothing, not ready to advance
                }
            }
            return nextLine;
        }

        public virtual void GoBack()
        {
            ResetDeck();
            myScript.GoBack();
        }
    }
}