using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Vortex
{
    using Utility.Audio;

    public class ScriptPlayer : MonoBehaviour
    {
        public static int MAX_WORDS_PER_CARD = 25; // won't work if words are longer than average

        public static float NARRATION_BUFFER_SECONDS
        {
            get { return 1.0f; }
        }
        public static float NOTHING_TO_PLAY
        {
            get { return -NARRATION_BUFFER_SECONDS; }
        }
        public static float CURRENTLY_PLAYING
        {
            //get { return Mathf.NegativeInfinity; }
            get { return 0.0f; }
        }


        public SpriteRenderer m_screenSprite;

        private TextMeshProUGUI textOutput;
        private AudioSource m_narrationPlayer;

        public string WelcomeText;

        private float lastDuration;
        private string lastDisplay;

        private bool ignoreInputs = false;

        public bool IsPlaying
        {
            get { return m_narrationPlayer.isPlaying; }
        }

        // Start is called before the first frame update
        void Start()
        {
            textOutput = GetComponent<TextMeshProUGUI>();
            m_narrationPlayer = GetComponent<AudioSource>();

            if (AudioManager.m_Instance)
            {
                AudioManager.RegisterSource(ref m_narrationPlayer);
            }

            if (textOutput)
            {
                SetDisplayText(WelcomeText);
            }
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void TurnOff()
        {
            Stop();
            ignoreInputs = true;
        }

        public void Stop()
        {
            m_narrationPlayer.Stop();
            ClearDisplay();
        }

        private void SetDisplayText(string text)
        {
            if (!ignoreInputs)
            {
                lastDisplay = textOutput.text;
                textOutput.text = text;
            }
        }

        public float PlayLine(LineFile line)
        {
            float playtime = NOTHING_TO_PLAY;
            if (IsPlaying && !line.Interrupt)
            {
                //playtime = CURRENTLY_PLAYING;
                playtime = -lastDuration;
            }
            else if (line)
            {
                if (!string.IsNullOrEmpty(line.Line))
                {
                    SetDisplayText(line.Line);
                }

                if (line.Record)
                {
                    if (line.Interrupt)
                    {
                        m_narrationPlayer.Stop();
                    }

                    m_narrationPlayer.PlayOneShot(line.Record, AudioManager.VOLUME);
                    playtime = line.Record.length;
                    lastDuration = playtime;

                    // Scientist talk animation
                    if (line.IsOffMic)
                    {
                        Scientist.OnTalkMic_Away?.Invoke(playtime);
                    }
                    else
                    {
                        Scientist.OnTalkMic_Into?.Invoke(playtime);
                    }

                    // Clear once line is done
                    if (line.AfterPlay == LineFile.AfterPlayEnum.CLEAR)
                    {
                        Invoke("ClearDisplay", playtime);
                    }
                    else if (line.AfterPlay == LineFile.AfterPlayEnum.REVERT)
                    { 
                        Invoke("RevertDisplay", playtime);
                    }
                }
            }

            return playtime;
        }

        private void RevertDisplay()
        {
            textOutput.text = lastDisplay;
        }

        private void ClearDisplay()
        {
            textOutput.text = string.Empty;
        }

        public bool ToggleMute()
        {
            return (m_narrationPlayer.mute = !m_narrationPlayer.mute);
        }
    }
}