using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vortex
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Projectile : MonoBehaviour
    {
        public static ProjectileEventHandler OnRestoreGravity;

        private Rigidbody2D m_rigidBody;
        private Collider2D m_collider;

        public float Mass = 0.2f;
        private float defaultDrag;
        private float defaultGravity;

        public PhysicsMaterial2D airborneMaterial;
        public PhysicsMaterial2D groundedMaterial;

        private Vacuum m_Vacuum;

        public float squishVelocity = 8.0f;
        public float squishAmount = 0.75f;
        public float lerpDuration = 0.1f;
        private Vector3 FULL_SCALE = Vector3.one;
        private Vector3 SQUISH_SCALE;
        private float timeElapsed;

        [SerializeField] private List<AudioClip> collisionSounds;

        public enum State
        {
            AIRBORNE,
            GROUNDED
        };

        public static int groundLayer;
        public static int airborneLayer;
        public static int groundedLayer;

        // where the object was released
        // Marks where object should become grounded again
        private float releaseY;

        private State currentState;
        public State CurrentState
        {
            get { return currentState; }
            set
            {
                if (value == State.GROUNDED && currentState != State.GROUNDED)
                {
                    Ground();
                }

                currentState = value;
            }
        }

        private void setLayerMask(int layer)
        {
            // reset
            gameObject.layer &= 0;
            gameObject.layer = layer;
        }

        public bool isFalling
        {
            get 
            {
                return (m_rigidBody.velocity.y < 0);
            }
        }

        public bool isMovingFast
        {
            get
            {
                return (m_rigidBody.velocity.magnitude >= squishVelocity);
            }
        }

        public bool isTouchingGround
        {
            get
            {
                return (m_collider.IsTouchingLayers(groundLayer));
            }
        }

        private void OnEnable()
        {
            OnRestoreGravity += RestoreGravity;
        }

        private void OnDisable()
        {
            OnRestoreGravity -= RestoreGravity;
        }

        void Awake()
        {
            m_rigidBody = GetComponent<Rigidbody2D>();
            m_collider = GetComponent<Collider2D>();

            groundLayer = 1 << LayerMask.NameToLayer("Ground");
            airborneLayer = 1 << LayerMask.NameToLayer("Projectile");
            groundedLayer = LayerMask.NameToLayer("Grounded Projectile");

            SQUISH_SCALE = new Vector3(squishAmount, 1.0f, 1.0f);
            defaultDrag = m_rigidBody.drag;
            defaultGravity = m_rigidBody.gravityScale;
        }

        // Take care of physics things
        void FixedUpdate()
        {
            if(isFalling && isTouchingGround && transform.position.y <= releaseY)
            {
                CurrentState = State.GROUNDED;
            }

            // Apply vacuum force
            if (m_Vacuum)
            {
                AddForce(m_Vacuum.GetForce(this), ForceMode2D.Force);
            }
        }

        private void Update()
        {
            // squish and look at velocity
            if (isMovingFast)
            {
                Vector3 heading = m_rigidBody.velocity;
                transform.Rotate(0f, 0f, Mathf.Atan2(heading.y, heading.x) * Mathf.Rad2Deg); // this is wonky, but other stuff isn't working.
                //Debug.Log("Velocity = " + heading);
                //Quaternion newRot = transform.rotation;
                //newRot.z = Mathf.Atan2(heading.y, heading.x) * Mathf.Rad2Deg;
                //transform.rotation = newRot;
                //Vector3 lookAt = m_rigidBody.velocity;
                //Debug.Log("Look at " + lookAt);
                //Vector3 reference = Vector3.Cross(Vector3.forward, Vector3.Cross(Vector3.forward, Vector3.up));
                //transform.rotation = Quaternion.LookRotation(lookAt, Vector3.forward);

                if (transform.localScale != SQUISH_SCALE)
                {
                    transform.localScale = Vector3.Lerp(FULL_SCALE, SQUISH_SCALE, timeElapsed / lerpDuration);
                    timeElapsed += Time.deltaTime;
                }
                // done lerping, reset clock
                else
                {
                    timeElapsed = 0;
                }
            }
            // restore scale
            else
            {
                if (transform.localScale != FULL_SCALE)
                {
                    transform.localScale = Vector3.Lerp(SQUISH_SCALE, FULL_SCALE, timeElapsed / lerpDuration);
                    timeElapsed += Time.deltaTime;
                }
                // done lerping, reset clock
                else
                {
                    timeElapsed = 0;
                }
            }
        }

        private void Ground()
        {
            // set m_rigidBody settings to simulate being on the ground
            m_rigidBody.gravityScale = 0;
            m_rigidBody.drag = 4;
            m_rigidBody.sharedMaterial = groundedMaterial;

            setLayerMask(groundedLayer);

            PlayCollision();
        }

        public void Launch(Vector2 velocity)
        {
            releaseY = transform.position.y;
            AddForce(velocity, ForceMode2D.Impulse);
        }

        private void AddForce(Vector2 force, ForceMode2D mode)
        {
            if (!m_rigidBody)
            {
                Debug.LogError("Rigid body not init!");
                return;
            }

            switch (mode)
            {
                case ForceMode2D.Impulse:
                    m_rigidBody.velocity = force;
                    break;
                case ForceMode2D.Force:
                default:
                    m_rigidBody.velocity += force * Time.fixedDeltaTime;
                    break;
            }
        }

        public void Attract(Vacuum vacuum)
        {
            m_Vacuum = vacuum;
            // Ignore world gravity and slow, because this projectile will now be attracted by the vacuum
            m_rigidBody.gravityScale = 0;
            m_rigidBody.drag = vacuum.DragCoefficient;
        }

        private void RestoreGravity()
        {
            if (CurrentState == State.AIRBORNE)
            {
                m_rigidBody.gravityScale = defaultGravity;
            }

            m_rigidBody.drag = defaultDrag;
        }

        private void PlayCollision()
        {
            Utility.Audio.AudioManager.PlayRandomSound(ref collisionSounds, Utility.Audio.AudioManager.VOLUME_SFX * 0.5f);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Collider2D c = collision.collider;
            if (!Player.IsPlayer(c) &&
                !c.gameObject.GetComponent<Projectile>() &&
                !(c.name == "Back") &&
                !(c.name == "Bottom"))
            {
                PlayCollision();
            }
        }
    }

    public delegate void ProjectileEventHandler();
}